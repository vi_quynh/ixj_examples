<%-- 
    Document   : index
    Created on : May 21, 2018, 3:29:40 PM
    Author     : viquy
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html public "-//W3C//DTD HTML 4.01 Transitional//EN"
    "http://www.w3.org/TR/html4/loose.dtd">
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Index Page</title>
    </head>
    <body>
        ${info}
        <form action="ActionProcess" method="POST">
            <input type="hidden" name="act" value="getdata"/><input type="submit" value="Get Data"/>
        </form>
        <form action="ActionProcess" method="POST">
            <input type="hidden" name="act" value="showdata"/><input type="submit" value="Show data"/>
        </form>
        <form action="ActionProcess" method="POST">
            <input type="text" name="filter" value=""/>
            <input type="hidden" name="act" value="filteddata"/><input type="submit" value="Filted Data"/>
        </form>
    </body>
</html>
