<%-- 
    Document   : product
    Created on : May 21, 2018, 3:30:02 PM
    Author     : viquy
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<?xml-stylesheet type="text/xsl" href="product.xsl"?>
<Products>
    <c:forEach var="product" items="${products}">
        <Product>
        <ProductID>${product.productID}</ProductID>
        <ProductName>${product.productName}</ProductName>
        <SupplierID>${product.supplierID}</SupplierID>
        <CategoryID>${product.categoryID}</CategoryID>
        <QuantityPerUnit>${product.quantityPerUnit}</QuantityPerUnit>
        <UnitPrice>${product.unitPrice}</UnitPrice>
        <UnitsInStock>${product.unitsInStock}</UnitsInStock>
        <UnitOnOrder>${product.unitsOnOrder}</UnitOnOrder>
        <ReoderLevel>${product.reoderLevel}</ReoderLevel>
        <Discontinued>${product.discontinued}</Discontinued>
        </Product>
    </c:forEach>
</Products>
